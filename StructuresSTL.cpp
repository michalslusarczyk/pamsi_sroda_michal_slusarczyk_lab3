#include <stack>
#include <queue>
#include "Exceptions.cpp"
using namespace std;

template <typename T>
class StackSTL {
public:
    int getSize(){
        return size;
    }

    bool isEmpty(){
        return size == 0;
    }

	void push(T t) {
		s.push(t);
		size++;
	}

	T top(){
        return s.top();
	}

	T pop() {
		if (s.empty())
			throw EmptyStackException();
		else{
            T t = s.top();
			s.pop();
			size--;
            return t;
		}
	}

	void removeAll() {
        while (!s.empty())
            s.pop();
	}

	void print() {
		if (s.empty())
			throw EmptyStackException();
		else {
			stack<T> tmp;
			while (!s.empty()) {
				cout << s.top() << "\n";
				tmp.push(s.top());
				s.pop();
			}
			while (!tmp.empty()) {
				s.push(tmp.top());
				tmp.pop();
			}
		}
	}

private:
	stack<T> s;
    int size = 0;
};

template <typename T>
class QueueSTL{
public:
    void enqueue(T t){
        q.push(t);
        size++;
    }

    T dequeue(){
        if(q.empty()){
            throw EmptyQueueException();
        }else{
            T t = q.front();
            q.pop();
            size--;
            return t;
        }
    }

    T front(){
        if(q.empty()){
            throw EmptyQueueException();
        }else{
            return q.front();
        }
    }

    int getSize(){
        return size;
    }

    bool isEmpty(){
        return size == 0;
    }

    void print(){
        if (q.empty())
			throw EmptyQueueException();
		else {
			queue<T> tmp;
			while (!q.empty()) {
				cout << q.top() << "\n";
				tmp.push(q.top());
				q.pop();
			}
			while (!tmp.empty()) {
				q.push(tmp.top());
				tmp.pop();
			}
		}
    }

    void removeAll(){
        while(!q.empty()){
            q.pop();
        }
    }

private:
    queue<T> q;
    int size = 0;
};
