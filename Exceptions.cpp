#include <iostream>
#include <string>

/*

Obiekty wyj�tk�w

*/
class EmptyStackException{
private:
    std::string value = "Stack is empty";
public:
    void printException(){
        std::cout<<value<<std::endl;
    }
};

class EmptyQueueException{
private:
    std::string value = "Queue is empty";
public:
    void printException(){
        std::cout<<value<<std::endl;
    }
};
