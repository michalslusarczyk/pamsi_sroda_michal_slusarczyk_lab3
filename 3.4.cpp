#include <iostream>
#include "Stack.cpp"

void hanoi(int n, Stack<int>* A, Stack<int>* B, Stack<int>* C){
    if(n > 0){
        hanoi(n-1, A, C, B);
        C->push(A->pop());
        hanoi(n-1, B, A, C);
    }
}

int main(){

    Stack<int>* wiezaA = new Stack<int>();
    Stack<int>* wiezaB = new Stack<int>();
    Stack<int>* wiezaC = new Stack<int>();

    int n = 3;

    for(int i = n; i>0; i--){
        wiezaA->push(i);
    }

    hanoi(n, wiezaA, wiezaB, wiezaC);

    while(!wiezaC->isEmpty()){
        std::cout<<wiezaC->pop()<<"\n";
    }

    return 0;
}
