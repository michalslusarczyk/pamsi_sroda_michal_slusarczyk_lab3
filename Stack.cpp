#include "Exceptions.cpp"
#include <iostream>

template <typename T>
class Stack{
public:
    Stack();
    void push(T t);
    T pop();
    T top();
    bool isEmpty();
    int getSize();

    void print();
    void removeAll();
private:

    //rozmiar tablicy
    int arraySize;

    //indeks pierwszego elementu stosu (czyli ostatniego w tablicy)
    int topIndex;

    //tablica
    T* stackArray;

    //sta�a inkrementacji
    int incrementConstant = 100;

    //strategia inkrementalna
    void resizeArray();

    //strategia podwajania
    void resizeArray2();
};

template <typename T>
Stack<T>::Stack(){
    topIndex = -1;
    arraySize = incrementConstant;
    stackArray = new T[arraySize];
}


template <typename T>
void Stack<T>::push(T t){
    if(topIndex+1 == arraySize){
        resizeArray2();
    }
    topIndex++;
    stackArray[topIndex] = t;
}

template <typename T>
T Stack<T>::pop(){
    if(isEmpty()){
        throw EmptyStackException();
    }
    else{
        T temp = stackArray[topIndex];
        stackArray[topIndex] = NULL;
        topIndex--;
        return temp;
    }
}

template <typename T>
bool Stack<T>::isEmpty(){
    return topIndex == -1;
}

template <typename T>
int Stack<T>::getSize(){
    return topIndex+1;
}

template <typename T>
T Stack<T>::top(){
    if(isEmpty())
        throw EmptyStackException();
    else
        return stackArray[topIndex];
}

template <typename T>
void Stack<T>::print(){
    for(int i = 0; i<getSize(); ++i){
        std::cout<<stackArray[i]<<"\n";
    }
}

template <typename T>
void Stack<T>::removeAll(){
    delete stackArray;
    stackArray = new T[incrementConstant];
}

template <typename T>
void Stack<T>::resizeArray(){
    T* stackArray2 = new T[arraySize + incrementConstant];
    for(int i = 0; i<arraySize; ++i){
        stackArray2[i] = stackArray[i];
    }
    arraySize+=incrementConstant;
    delete stackArray;
    stackArray = stackArray2;
}

template <typename T>
void Stack<T>::resizeArray2(){
    T* stackArray2 = new T[arraySize*2];
    for(int i = 0; i<arraySize; ++i){
        stackArray2[i] = stackArray[i];
    }
    arraySize*=2;
    delete stackArray;
    stackArray = stackArray2;
}
