//#include "Stack.cpp"
#include "Queue.cpp"
//#include "StructuresSTL.cpp"
#include <iostream>
#include <string>

#include <windows.h>
#include <cstdlib>
#include <ctime>

using namespace std;

LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}
LARGE_INTEGER endTimer()
{
LARGE_INTEGER stop;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&stop);
SetThreadAffinityMask(GetCurrentThread(), oldmask);
return stop;
}

int main(){

    srand(time(NULL));

    Queue<int>* kolejka = new Queue<int>();

    LARGE_INTEGER performanceCountStart,performanceCountEnd;
    performanceCountStart = startTimer();
    for(int i=0; i< 1000; i++){
        kolejka->enqueue(rand());
    }
    performanceCountEnd = endTimer();
    //cout<<"koniec? "<<stos->getSize()<<"\n";

    double tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
    cout << endl << "Time:" <<tm <<endl;

    return 0;
}
