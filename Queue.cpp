#include "Exceptions.cpp"

template <typename T>
class Queue{
public:
    Queue();
    void enqueue(T t);
    T dequeue();
    T front();

    int getSize();
    bool isEmpty();

    void print();
    void removeAll();
private:
    //indeks ostatniego elementu w tablicy
    int lastIndex;

    //indeks pierwszego elementu w tablicy
    int firstIndex;

    //liczba element�w w tablicy
    int elements;

    //rozmiar tablicy
    int arraySize;

    //tablica
    T* queueArray;

    //==========================

    //sta�a inkrementacji
    int incrementConstant = 100;
    //strategia inkrementalna
    void resizeArray();
    //strategia podwajania
    void resizeArray2();

};

template <typename T>
Queue<T>::Queue(){
    lastIndex = -1;
    firstIndex = 0;
    arraySize = incrementConstant;
    elements = 0;
    queueArray = new T[arraySize];
}

template <typename T>
void Queue<T>::enqueue(T t){
    if(lastIndex+1 == arraySize){
        resizeArray();
    }
    lastIndex++;
    elements++;
    queueArray[lastIndex] = t;
}

template <typename T>
T Queue<T>::dequeue(){
    if(isEmpty())
        throw EmptyQueueException();
    else{
        T temp = queueArray[firstIndex];
        queueArray[firstIndex] = NULL;
        firstIndex++;
        elements--;
        return temp;
    }
}

template <typename T>
T Queue<T>::front(){
    if(isEmpty())
        throw EmptyQueueException();
    else
        return queueArray[firstIndex];
}

template <typename T>
int Queue<T>::getSize(){
    return elements;
}

template <typename T>
bool Queue<T>::isEmpty(){
    return elements == 0;
}

template <typename T>
void Queue<T>::print(){
    for(int i = firstIndex; i<=lastIndex; ++i){
        std::cout<<queueArray[i]<<"\n";
    }
}

template <typename T>
void Queue<T>::removeAll(){
    delete queueArray;
    queueArray = new T[incrementConstant];
}

template <typename T>
void Queue<T>::resizeArray(){
    T* queueArray2 = new T[arraySize + incrementConstant];
    for(int i = 0; i<arraySize; ++i){
        queueArray2[i] = queueArray[i];
    }
    arraySize+=incrementConstant;
    delete queueArray;
    queueArray = queueArray2;
}

template <typename T>
void Queue<T>::resizeArray2(){
    T* queueArray2 = new T[arraySize*2];
    for(int i = 0; i<arraySize; ++i){
        queueArray2[i] = queueArray[i];
    }
    arraySize*=2;
    delete queueArray;
    queueArray = queueArray2;
}


