#include <iostream>
#include <string>

// WEZEL LISTY
//===============================
template <typename T>
class ListElement{
public:
    T getElement();
    void setElement(T element);
    ListElement<T>* getNext();
    void setNext(ListElement<T>* nxt);
private:
    T value;
    ListElement<T>* next;
};

template <typename T>
T ListElement<T>::getElement(){
    return value;
}

template <typename T>
void ListElement<T>::setElement(T element){
    value = element;
}

template <typename T>
ListElement<T>* ListElement<T>::getNext(){
    return next;
}

template <typename T>
void ListElement<T>::setNext(ListElement<T>* nxt){
    next = nxt;
}

// LISTA
//===========================================

template <typename T>
class List{
protected:
    ListElement<T>* first;
public:
    List();
    bool isEmpty();
    T& getFront();
    ListElement<T>* getFirst();
    void addFront(const T& t);
    ListElement<T>* removeFront();
    void display();
    void removeAll();
};

template <typename T>
List<T>::List(){
    first = nullptr;
}

template <typename T>
bool List<T>::isEmpty(){
    return first == nullptr;
}

template <typename T>
void List<T>::addFront(const T& t){
    ListElement<T>* v = new ListElement<T>;
    v->setElement(t);
    v->setNext(first);
    first = v;
}

template <typename T>
T& List<T>::getFront(){
    return first->getElement();
}

template <typename T>
ListElement<T>* List<T>::removeFront(){
    ListElement<T>* temp = first;
    first = temp->getNext();
    return temp;
}

template <typename T>
ListElement<T>* List<T>::getFirst(){
    return first;
}

template <typename T>
void List<T>::display(){
    ListElement<T>* temp = first;
    while(temp != nullptr){
        std::cout<<temp->getElement()<<"\n";
        temp = temp->getNext();
    }
}

template <typename T>
void List<T>::removeAll(){
    while(!isEmpty()){
        removeFront();
    }
}



// KOLEJKA
//================================================================


template <typename T>
class Queue:public List<T>{
public:
    Queue();
    void enqueue(T value);
    T dequeue();
    void setLast(ListElement<T>* elem);
    ListElement<T>* getLast();
private:
    ListElement<T>* last;
};

template <typename T>
Queue<T>::Queue(){
    this->first = nullptr;
    last = this->first;
}

template <typename T>
void Queue<T>::setLast(ListElement<T>* elem){
    last = elem;
}

template <typename T>
ListElement<T>* Queue<T>::getLast(){
    return last;
}

template <typename T>
void Queue<T>::enqueue(T t){
    ListElement<T>* v = new ListElement<T>();
    v->setElement(t);
    v->setNext(nullptr);
    if(this->isEmpty()){
        this->first = v;
        last = this->first;
    }else{
        last->setNext(v);
        last = v;
    }
}

template <typename T>
T Queue<T>::dequeue(){
   ListElement<T>* v = new ListElement<T>();
   v = this->first;
   this->first = this->first -> getNext();
   return v->getElement();
}


//WEZEL DWUKIERUNKOWY

template <typename T>
class DequeElement{
public:
    T getElement();
    void setElement(T element);

    DequeElement<T>* getNext();
    void setNext(DequeElement<T>* nxt);
    DequeElement<T>* getPrev();
    void setPrev(DequeElement<T>* prv);
private:
    T value;
    DequeElement<T>* prev;
    DequeElement<T>* next;
};

template <typename T>
T DequeElement<T>::getElement(){
    return value;
}

template <typename T>
void DequeElement<T>::setElement(T element){
    value = element;
}

template <typename T>
DequeElement<T>* DequeElement<T>::getNext(){
    return next;
}

template <typename T>
void DequeElement<T>::setNext(DequeElement<T>* nxt){
    next = nxt;
}

template <typename T>
DequeElement<T>* DequeElement<T>::getPrev(){
    return prev;
}

template <typename T>
void DequeElement<T>::setPrev(DequeElement<T>* prv){
    prev = prv;
}


// DEQUE
//================================================================

template <typename T>
class Deque{
public:
    const std::string DequeEmptyException = "DEQUE JEST PUSTY";

    Deque();

    int getSize();

    void addFront(T t);
    void addRear(T t);

    T getFront();
    T getRear();

    T removeFront();
    T removeRear();

private:
    int sizeOfDeque;
    DequeElement<T>* first;
    DequeElement<T>* rear;
};

template <typename T>
Deque<T>::Deque(){
    sizeOfDeque = 0;
    first = nullptr;
    rear = nullptr;
}

template <typename T>
int Deque<T>::getSize(){
    return sizeOfDeque;
}

template <typename T>
T Deque<T>::getFront(){
    if(sizeOfDeque == 0)
        throw DequeEmptyException;
    else
        return first->getElement();
}

template <typename T>
T Deque<T>::getRear(){
    if(sizeOfDeque == 0)
        throw DequeEmptyException;
    else
        return rear->getElement();
}

template <typename T>
void Deque<T>::addFront(T t){
    DequeElement<T>* v = new DequeElement<T>;
    v->setElement(t);
    v->setNext(nullptr);
    v->setPrev(nullptr);
    if(sizeOfDeque == 0){
        first = v;
        rear = v;
    }else{
        first->setPrev(v);
        v->setNext(first);
        first = v;
    }
    ++sizeOfDeque;
}

template <typename T>
void Deque<T>::addRear(T t){
    DequeElement<T>* v = new DequeElement<T>;
    v->setElement(t);
    v->setNext(nullptr);
    v->setPrev(nullptr);
    if(sizeOfDeque == 0){
        first = v;
        rear = v;
    }else{
        rear->setNext(v);
        v->setPrev(rear);
        rear = v;
    }
    ++sizeOfDeque;
}

template <typename T>
T Deque<T>::removeFront(){
    if(sizeOfDeque == 0){
        throw DequeEmptyException;
    }else{
        T value = first->getElement();
        DequeElement<T>* temp = first;
        if(sizeOfDeque == 1){
            first = nullptr;
            rear = nullptr;
        }else{
            first -> getNext() -> setPrev(nullptr);
            first = first -> getNext();
        }
        --sizeOfDeque;
        delete temp;
        return value;
    }
}

template <typename T>
T Deque<T>::removeRear(){
    if(sizeOfDeque == 0){
        throw DequeEmptyException;
    }else{
        T value = rear->getElement();
        DequeElement<T>* temp = rear;
        if(sizeOfDeque == 1){
            first = nullptr;
            rear = nullptr;
        }else{
            rear -> getPrev() -> setNext(nullptr);
            rear = rear -> getPrev();
        }
        --sizeOfDeque;
        delete temp;
        return value;
    }
}













